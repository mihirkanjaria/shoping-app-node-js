const path = require('path');

const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const session = require('express-session');
const MongoDBStore = require('connect-mongodb-session')(session);
const csrf = require('csurf');
const flash = require('connect-flash');
const multer = require('multer');

const errorController = require('./controllers/error');
const User = require('./models/user');
const helpers = require('./helpers');
const shopController = require('./controllers/shop');
const isAuth = require('./middleware/is-auth');

const MONGODB_URI = 'mongodb+srv://Mihir:mnkvision17@cluster0.mq41s.mongodb.net/shop?retryWrites=true&w=majority';

const app = express();

const store = MongoDBStore({
  uri:MONGODB_URI,
  collection:'sessions'
})

const csrfProtection = csrf();

const fileStorage = multer.diskStorage({
  destination: function(req,file,cb) { // req : simple request file: file which is using the parser cb: Once we done with our logic call the cb function to contiune
    cb(null,'images'); // here null meaning no error is thrown/it's okay to store
  },
  filename : function(req,file,cb){
    cb(null,file.fieldname + '-' + Date.now() + '-' + file.originalname); // make sure old file doesn't replace by the new one and we keep our extension as it is
  }
})

const fileFilter = (req,file,cb)=>{
  if(file.mimetype==='image/png' || file.mimetype==='image/jpg' ||file.mimetype==='image/jpeg')
  {
    cb(null,true);
  }else
  {
    cb(null,false);
  }
}

app.set('view engine', 'ejs');
app.set('views', 'views');

const authRoutes = require('./routes/auth');
const adminRoutes = require('./routes/admin');
const shopRoutes = require('./routes/shop');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(multer({storage:fileStorage, fileFilter:fileFilter}).single('image')); // for parsing multipart data like image in form here single refer to parse only a single file
app.use(express.static(path.join(__dirname, 'public')));
app.use('/images',express.static(path.join(__dirname, 'images'))); // all images inside this folder will be treated as they are in the root directory so we any request goes to slash image we are removing that part like the routes of admin
app.use(session({secret:'my secret',resave:false,saveUninitialized:false,store:store}));// secret used to sign in the hash which store id in the cookie resave and saveUninitialized false meaning the session will not be save on unchanged responses only save when something is changed


app.use(flash());

app.use((req,res,next)=>{
  //throw new Error('Sync Dummy'); // this will work like next(error) but only in the sync code
  if(!req.session.user)
  {
    return next();
  }
  User.findById(req.session.user._id).then(user=>{
    //throw new Error('Async Dummy');
    if(!user)
    {
      next();
    }
    req.user = user;
    next();
  }).catch(err=> {
    //throw new Error(err) //This will simply throw the error will call next(error) because it is in the asynchronous code
    next(new Error(err));
  });
});


app.use((req,res,next)=>{
  res.locals.isAuthenticated = req.session.isLoggedIn;
  next();
})

app.post('/create-order', isAuth, shopController.postOrder);
app.use(csrfProtection);

app.use((req,res,next)=>{
  res.locals.csrfToken = req.csrfToken(); // enabling csrf token after the payment method because stripe has it own security mechanism
  next();
})

app.use('/admin', adminRoutes);
app.use(shopRoutes);
app.use(authRoutes);

app.get('/500',errorController.get500);
app.use(errorController.get404);

app.use((error,req,res,next)=>{
  //res.redirect('/500'); // This will become infinite loop every redirect acts like a new incoming request so it will comes in this block will have maunally added a error inside the req.session.user block above so we need to render it
  console.log(error);
  res.status(500).render('500', { pageTitle: 'Technical Error Occured', path: '/500',
  isAuthenticated : req.isLoggedIn });
})

mongoose.connect(MONGODB_URI)
.then(result=>{
  console.log("Connected !!!");
  app.listen(3000);
}).catch(err=>console.log(err));

