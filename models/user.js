const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const UserSchema = new Schema({
  password:{
    type:String,
    required:true
  },
  email:{
    type:String,
    required:true
  },
  resetToken:String,
  resetTokenExpiration:Date,
  cart:{
    items:[
      {productId:{
        type:Schema.Types.ObjectId,
        required:true,
        ref:'Product'
      },
      quantity:{
        type:Number,
        required:true
      }}
    ]
  }
})

UserSchema.methods.addToCart = function(product){
    const cartProductIndex = this.cart.items.findIndex(cp=>{
      return cp.productId.toString() === product._id.toString();
    });
    const updatedCartItems = [...this.cart.items];
    let newQuantity = 1;
    if(cartProductIndex>=0)
    {
      newQuantity = this.cart.items[cartProductIndex].quantity + 1;
      updatedCartItems[cartProductIndex].quantity = newQuantity;
    }else
    {
      updatedCartItems.push( {productId: product._id, quantity:newQuantity})
    }
    const updatedCart = {items:updatedCartItems};
    //console.log(updatedCart);
    this.cart = updatedCart;
    return this.save();

}

UserSchema.methods.removeFromCart = function(productId){
  const updateCartItems = this.cart.items.filter(item=>{
          return item.productId.toString() !== productId.toString();
        });
  this.cart.items = updateCartItems;
  return this.save();
}
UserSchema.methods.clearCart = function(){
  this.cart = {items:[]}
  return this.save();
}

module.exports = mongoose.model('User',UserSchema);
