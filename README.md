# Description of the project

## This project is part of Maximilian Schwarzmüller Course of Node JS on Udemy.
(Link: https://www.udemy.com/course/nodejs-the-complete-guide/)

Basically this project is about a shopping Application in which:- 
- User can Authentication
- User can place a order
- User can manage product and order
- User can view, update and delete the projects

There are two role in the User Model : Normal User and Admin User

Langauge used : For Backend Development Node JS, Express JS and MongoDB
And Frontend is developed using ejs templating engine, html, css and JS

Command to run the app : npm start (in your respective app directory).
It is deployed on localhost:3000 as of now. but it will be deployed online once it get completed.
