const path = require('path');

const express = require('express');
const {body} = require('express-validator/check');

const adminController = require('../controllers/admin');
const isAuth = require('../middleware/is-auth');


const router = express.Router();

 // /admin/add-product => GET
router.get('/add-product', isAuth, adminController.getAddProduct); // parsing is done from left to right so first thing is we check if the user is authenticated or not and then we pass the control to the controller

// /admin/products => GET
router.get('/products', isAuth, adminController.getProducts);

// /admin/add-product => POST
router.post('/add-product', [
body('title')
.isString()
.isLength({min:3})
.trim(),
// body('imageUrl')
// .isURL(),
body('price')
.isFloat(),
body('description')
.isLength({min:5,max:400})
],
isAuth, adminController.postAddProduct);

router.get('/edit-product/:productId', isAuth, adminController.getEditProduct);

router.post('/edit-product',[
    body('title')
    .isString()
    .isLength({min:3})
    .trim(),
    // body('imageUrl')
    // .isURL(),
    body('price')
    .isFloat(),
    body('description')
    .isLength({min:5,max:400})
    ], isAuth, adminController.postEditProduct);

router.delete('/product/:productId', isAuth, adminController.deleteProduct); // body is not allowed in the delete request
module.exports = router;
