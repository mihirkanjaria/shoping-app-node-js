const express = require('express');
const { check,body } = require('express-validator/check');
const User = require('../models/user');

const authController = require('../controllers/auth');

const router = express.Router();

router.get('/login', authController.getLogin);

router.get('/signup', authController.getSignup);

router.post('/signup', [
    check('email')
    .isEmail()
    .withMessage('Please Enter a valid email !!')
    .custom((value,{req})=>{
    // if(value==='mihir.k@ahduni.edu.in')
    // {
    //     throw new Error('This email id is forbidden please enter any other valid email id !!!')
    // }
    // return true;
    return User.findOne({email:value}).then(user=>{
    if(user)
    {
        return Promise.reject('Email Already Exists !!!')
    } })
    }).normalizeEmail(),body('password','Please enter a password which must be alphanumeric and have length of atleast 5')
    .isLength({min:5})
    .isAlphanumeric()
    .trim(),
    body('confirmPassword').trim().custom((value,{req})=>{
    if(value!==req.body.password)
    {
        throw new Error('Password have to matched!!!');
    }
    return true
    })], authController.postSignup)
/*
we can group our check express validator in the array or seperated by comma.
We can import checker that check value of specific item like body, header, cookie, param, query but check will look into all part and find the element's value
In a case where the error message is repeating we can give as a second argument in the express-validator
*/

/*
We can add async validator in the custom of express validator

*/

router.post('/login',[body('email').isEmail().withMessage('Please enter a valid email address.').normalizeEmail(),body('password','Password has to be valid.').isAlphanumeric().isLength({min:5}).trim()],authController.postLogin);

router.post('/logout',authController.postLogout);

router.get('/reset',authController.getReset);

router.post('/reset',authController.postReset);

router.get('/reset/:token',authController.getNewPassword);

router.post('/new-password',authController.postNewPassword);

module.exports = router;