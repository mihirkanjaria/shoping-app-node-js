const fs = require('fs');

const deleteFile = (filepath) =>{
    fs.unlink(filepath,(err)=>{
        if(err)
        {
            return new Error(err);
        }
    })
}
exports.deleteFile = deleteFile;