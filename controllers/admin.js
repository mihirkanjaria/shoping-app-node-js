const { validationResult } = require('express-validator/check');
const mongodb  = require('mongodb');
const mongoose = require('mongoose');
const Product = require('../models/product');
const ObjectId = mongodb.ObjectId;
const fh = require('../util/file');

exports.getAddProduct = (req, res, next) => {
  res.render('admin/edit-product', {
    pageTitle: 'Add Product',
    path: '/admin/add-product',
    editing:false,
    hasError:false,
    errorMessage:null,
    validationErrors:[]
  });
};

exports.postAddProduct = (req, res, next) => {
  const title = req.body.title;
  const image = req.file;
  const price = req.body.price;
  const description = req.body.description; 
  const errors = validationResult(req);
  
  

  if(!image)
  {
    return res.status(422).render('admin/edit-product', {
      pageTitle: 'Add Product',
      path: '/admin/add-product',
      editing:false,
      hasError:true,
      product:{
        title:title,
        price:price,
        description:description,
      },
      errorMessage:"Attached file is not a image !!!",
      validationErrors:errors.array()
    });
  }

  if(!errors.isEmpty())
  {
    return res.status(422).render('admin/edit-product', {
      pageTitle: 'Add Product',
      path: '/admin/add-product',
      editing:false,
      hasError:true,
      product:{
        title:title,
        price:price,
        description:description,
        imageUrl:image.path
      },
      errorMessage:errors.array()[0].msg,
      validationErrors:errors.array()
    });
  }

  const product = new Product({
    title:title, 
    price:price, 
    description:description, 
    imageUrl:image.path,
    userId:req.user});
  product
    .save()
    .then(result => {
      // console.log(result);
      console.log('Created Product');
      res.redirect('/admin/products');
    })
    .catch(err => {
      console.log('Somethinf Fishy!!');
      const error = new Error('Database Opertaion Failed');
      error.httpStatusCode = 500;
      return next(error);
      // return res.status(500).render('admin/edit-product', {
      //   pageTitle: 'Add Product',
      //   path: '/admin/add-product',
      //   editing:false,
      //   hasError:true,
      //   product:{
      //     title:title,
      //     price:price,
      //     description:description,
      //     imageUrl:imageUrl
      //   },
      //   errorMessage:'Technical Error Occured!!',
      //   validationErrors:[]
      // });
    });
};

exports.getEditProduct = (req, res, next) => {
  const editMode = req.query.edit;
  if (!editMode) {
    return res.redirect('/');
  }
  const prodId = req.params.productId;
    Product.findById(prodId)
    .then(product => {
      if (!product) {
        return res.redirect('/');
      }
      res.render('admin/edit-product', {
        pageTitle: 'Edit Product',
        path: '/admin/edit-product',
        editing: editMode,
        product: product,
        hasError:false,
        errorMessage:null,
        validationErrors:[],

      });
    })
    .catch(err => {
      const error = new Error('Database Opertaion Failed');
      error.httpStatusCode = 500;
      return next(error);
    });
};

exports.postEditProduct = (req, res, next) => {
  const prodId = req.body.productId;
  const updatedTitle = req.body.title;
  const updatedPrice = req.body.price;
  const image = req.file;
  const updatedDesc = req.body.description;
  console.log(updatedPrice);
  const errors = validationResult(req);
  if(!errors.isEmpty())
  {
    console.log(errors.array());
    return res.status(422).render('admin/edit-product', {
      pageTitle: 'Edit Product',
      path: '/admin/edit-product',
      editing:true,
      hasError:true,
      product:{
        title:updatedTitle,
        price:updatedPrice,
        description:updatedDesc,
        _id:prodId
      },
      errorMessage:errors.array()[0].msg,
      validationErrors:errors.array()
    });
  }

  Product.findById(prodId).then(product=>{
    if(product.userId.toString() !== req.user._id.toString())
    {
      return res.redirect('/');
    }
    product.title = updatedTitle;
    product.price = updatedPrice;
    if(image)
    {
      fh.deleteFile(product.imageUrl);
      product.imageUrl = image.path;
    }
    product.description = updatedDesc;
    product.save().then(result=>{
      res.redirect('/admin/products');
    }).catch(err=>console.log(err));
  })
};

exports.getProducts = (req, res, next) => {
  Product.find()
 // .select('title price -_id') // Used to select only required fields from the data
  //.populate('userId') // It will fetch the user information along the data of products
    .then(products => {
      console.log(products);
      res.render('admin/products', {
        prods: products,
        pageTitle: 'Admin Products',
        path: '/admin/products',
      });
    })
    .catch(err => {
      const error = new Error('Database Opertaion Failed');
      error.httpStatusCode = 500;
      return next(error);
    });
};

exports.deleteProduct = (req, res, next) => {
  const prodId = req.params.productId;
  Product.findById(prodId).then(product=>{
    fh.deleteFile(product.imageUrl);
    return  Product.deleteOne({_id:prodId, userId:req.user._id}) // To avoid Race condition on Product Model
  }).then(() => {
      console.log('DESTROYED PRODUCT');
      res.status(200).json({message:'Success'}) // diff between json and java object is that in json key needs to be define in double quotation marks
    })
    .catch(err => {
      res.status(500).json({message:'Failed'})
    });
};
