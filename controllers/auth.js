const User = require('../models/user');
const bcrypt = require('bcryptjs');
const nodemailer = require('nodemailer');
const sendgridTransport = require('nodemailer-sendgrid-transport');
const crypto = require('crypto');
const { validationResult } = require('express-validator/check');


const transporter = nodemailer.createTransport(sendgridTransport({
  auth:{
    api_key:'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'
  }
}))

exports.getLogin = (req, res, next) => {
  console.log(req.session.isLoggedIn);
  let message = req.flash('error');
  if(message.length >0)
  {
    message = message[0];
  }else
  {
    message = null; // null required because fronted display a error box with no text inside it.
  }
  res.render('auth/login', {
    path: '/login',
    pageTitle: 'Login',
    errorMessage:message,
    oldInput : {
      email:"",
      password:undefined,
    },
    validationErrors : []
  });
};

exports.getSignup = (req, res, next) => {
  let message = req.flash('error');
  if(message.length >0)
  {
    message = message[0];
  }else
  {
    message = null; // null required because fronted display a error box with no text inside it.
  }
  res.render('auth/signup', {
    path: '/signup',
    pageTitle: 'Signup',
    errorMessage:message,
    oldInput : {
      email:"",
      password:undefined,
      confirmPassword:undefined,
    },
    validationErrors : []
  });
};

exports.postLogin = (req,res,next)=>{
  const email =req.body.email;
  const password = req.body.password;
  const errors =  validationResult(req);

  if(!errors.isEmpty())
  {
    return res.status(422).render('auth/login',{
      path: '/login',
      pageTitle: 'Login',
      errorMessage: 'Invalid Email or Password !!',
      oldInput: {
        email: email,
        password: password
      },
      validationErrors: errors.array()
    });
  }
  User.findOne({ email: email })
  .then(user => {
    if (!user) {
      return res.status(422).render('auth/login',{
        path: '/login',
        pageTitle: 'Login',
        errorMessage: 'Invalid Email or Password !!',
        oldInput: {
          email: email,
          password: password
        },
        validationErrors: errors.array()
      });
    }
    bcrypt.compare(password,user.password)
    .then(doMatch=>{
      if(doMatch)
      {
        req.session.user = user;
        req.session.isLoggedIn = true;
        return req.session.save((err)=>{ // This does not required but it gurrantees that session is created in the database and then only the we redirect
      console.log(err);
      res.redirect('/'); // After the redirection data will be fetch from session collection so the method and functions will not work
      })
    }
    return res.status(422).render('auth/login',{
      path: '/login',
      pageTitle: 'Login',
      errorMessage: 'Invalid Email or Password !!',
      oldInput: {
        email: email,
        password: password
      },
      validationErrors: errors.array()
    });
    }).catch(err=>{console.log(err),
      res.redirect('/login');});
  }).catch(err => {
    const error = new Error('Database Opertaion Failed');
    error.httpStatusCode = 500;
    return next(error);
  });
  //req.isLoggedIn = true; // This data will be lost because when redirect we are creating a new incoming request which will not havr this field so value of it is undefined which is false
  //res.setHeader('Set-Cookie','loggedIn=true'); // Domain= to send cookie, HttpOnly where JS of client side will not be able to read the cookie, Max-Age to limit no. of seconds cookie should survive,(similar like Expires), Secure for HTTPS servers  
}

exports.postSignup = (req, res, next) => {
  const email = req.body.email;
  const password = req.body.password;
  const confirmPassword = req.body.confirmPassword;
  const errors = validationResult(req)
  if(!errors.isEmpty())
  {
    console.log(errors.array());
    return res.status(422).render('auth/signup', { // status 422 is used for failed validation
      path: '/signup',
      pageTitle: 'Signup',
      errorMessage:errors.array()[0].msg,
      oldInput : {
        email:email,
        password:password,
        confirmPassword:confirmPassword
      },
      validationErrors : errors.array()
    });
  }
  bcrypt.hash(password,12).then(cp=>{
      const user1 = new User(
        {email:email,
        password:cp,
      cart:{items:[]}});
      return user1.save();
    }).then(user=>{
      res.redirect('/');
      return transporter.sendMail({
        to:email,
        from:'kanjariamihir24@gmail.com',
        subject:'Successfull Signed Up :)',
        html:'<h1> Happy to have you </h1>'
      }).catch(err=>console.log(err));
  }).catch(err => {
    const error = new Error('Database Opertaion Failed');
    error.httpStatusCode = 500;
    return next(error);
  });
};

exports.postLogout = (req,res,next)=>{
  req.session.destroy(err=>{ // this is remove the session data but in browser you will the connect.sid but it will not match with any entry in the database so that session id is useless
    console.log(err);
    res.redirect('/');
  })
}

exports.getReset = (req,res,next)=>{
  let message = req.flash('error');
  if(message.length >0)
  {
    message = message[0];
  }else
  {
    message = null; // null required because fronted display a error box with no text inside it.
  }
  res.render('auth/reset', {
    path: '/reset',
    pageTitle: 'Reset Password',
    errorMessage:message
  }); 
}

exports.postReset = (req,res,next)=>{
  crypto.randomBytes(32,(err,buffer)=>{
    if(err)
    {
      console.log(err);
      return res.redirect('/reset');
    }
    const token = buffer.toString('hex');
    
  User.findOne({email:req.body.email}).then(user=>{
    if(!user)
    {
      req.flash('error','No account with that email exists !!!')
      return res.redirect('/reset');
    }
    user.resetToken = token;
    user.resetTokenExpiration = Date.now() + 3600000;
    return user.save()
  }).then(result=>{
    return transporter.sendMail({
      to:req.body.email,
      from:'kanjariamihir24@gmail.com',
      subject:'Password Reset Request !!!',
      html:`
      <p>You requested for a password reset</p>
      <p> Click this <a href='http://localhost:3000/reset/${token}'> link</a> to set a new password. </p>
      `
    })
  }).catch(err => {
    const error = new Error('Database Opertaion Failed');
    error.httpStatusCode = 500;
    return next(error);
  });
  })
}

exports.getNewPassword = (req,res,next)=>{
  const token = req.params.token;
  User.findOne({resetToken:token,resetTokenExpiration:{$gt:Date.now()}})
  .then(user=>{
    
    if(!user)
    {
      res.redirect('/reset');
    }

    let message = req.flash('error');
  if(message.length >0)
  {
    message = message[0];
  }else
  {
    message = null; // null required because fronted display a error box with no text inside it.
  }
  res.render('auth/new-password', {
    path: '/new-password',
    pageTitle: 'New Password',
    errorMessage:message,
    userId:user._id.toString(),
    passwordToken:token
  });
  }).catch(err=>console.log(err));
}

exports.postNewPassword = (req,res,next)=>{
  const userId = req.body.userId;
  const token = req.body.passwordToken;
  const updatedPassword = req.body.password;
  let updatedUser;

  User.findOne({resetToken:token,resetTokenExpiration:{$gt:Date.now()},_id:userId})
  .then(user=>{
    if(!user)
    {
      res.redirect('/reset');
    }
    updatedUser = user;
    return bcrypt.hash(updatedPassword,12)
  }).then(hasedPassword=>{
    updatedUser.password = hasedPassword;
    updatedUser.resetToken = undefined;
    updatedUser.resetTokenExpiration = undefined;
    return updatedUser.save()
  }).then(result=>{
    res.redirect('/login');
  }).catch(err => {
    const error = new Error('Database Opertaion Failed');
    error.httpStatusCode = 500;
    return next(error);
  });
}